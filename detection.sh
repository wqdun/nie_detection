#!/bin/bash
# clear
g_dir_old=$(pwd)
g_user=$(whoami)
g_script_fullpath=$(cd $(dirname $0) && pwd)
g_script_name=$(basename $0)
g_erlang_soft_fullpath=$(cd ${g_script_fullpath}/../ && pwd)

# eg., /home/ibd02/japan2018-12/1001-1-30009-181210/
g_task_fullpath=$1
g_log_fullpath="${g_task_fullpath}/Log/"
mkdir -p "${g_log_fullpath}"
g_result_log="${g_log_fullpath}/${g_script_name}.log"

log_with_time() {
    local now_time=$(date +%Y/%m/%d-%H:%M:%S)
    echo "$now_time: $*" >>$g_result_log
}

generate_rename_scripts() {
    log_with_time "$FUNCNAME start, param: $*"

    local task_fullpath=$1
    local image_fullpath="${task_fullpath}/Rawdata/Image/"
    (
        cd "${image_fullpath}"
        local rename_to_node_test_script="${g_log_fullpath}/rename_to_node_test.sh"
        if [ ! -f "${rename_to_node_test_script}" ]; then
            ls *.jpg | awk '{printf("mv %s node_test_%05d.jpg\n", $0, NR)}' >"${rename_to_node_test_script}"
        fi

        local rename_to_origin_script="${g_log_fullpath}/rename_to_origin.sh"
        if [ ! -f "${rename_to_origin_script}" ]; then
            awk '{print $1,$3,$2}' "${rename_to_node_test_script}" >"${rename_to_origin_script}"
        fi
    )
}

do_rename_to_node_test() {
    log_with_time "$FUNCNAME start, param: $*"

    local task_fullpath=$1
    local image_fullpath="${task_fullpath}/Rawdata/Image/"
    (
        cd "${image_fullpath}"
        bash "${g_log_fullpath}/rename_to_node_test.sh"
    )
}

do_rename_to_origin() {
    log_with_time "$FUNCNAME start, param: $*"

    local task_fullpath=$1
    local image_fullpath="${task_fullpath}/Rawdata/Image/"
    (
        cd "${image_fullpath}"
        bash "${g_log_fullpath}/rename_to_origin.sh"
    )
}

do_detection() {
    log_with_time "$FUNCNAME start, param: $*"
    local models_fullpath=$1
    local task_fullpath=$2
    local node_app_log=$3

    nvidia-docker run --rm \
        -v "${models_fullpath}":/models/:ro \
        -v "${task_fullpath}":/data/:rw \
        nvcr.io/navinfo/aicv_deliveries/node:19.04 \
        bash -c " \
            cd /data/Log/; \
            /usr/local/bin/node --backend tf --model_root /models/ \
                --camera sequence --sequence_prefix /data/Rawdata/Image/node_test_ \
                --file --timer; \
            cp results.txt detect_result.txt; \
        " >"${node_app_log}" 2>&1
}

update_progress_by_node_log() {
    log_with_time "$FUNCNAME start, param: $*"
    local node_app_log=$1
    local images_count=$2
    local progress_file=$3

    while [ true ]; do
        sleep 2
        update_progress "${node_app_log}" "${images_count}" "${progress_file}"
        if [ $? -eq 0 ]; then
            break
        fi
    done

    log_with_time "$FUNCNAME end."
}

# return: 0(finished); 1(processing)
update_progress() {
    log_with_time "$FUNCNAME start, param: $*"
    local node_app_log=$1
    local images_count=$2
    local progress_file=$3

    if [ ! -f "${node_app_log}" ]; then
        log_with_time "Failed to find ${node_app_log}."
        echo -ne "0\n${images_count}\nnull\n" >"${progress_file}"
        return 1
    fi

    local id=$(tail -n2 "${node_app_log}" | head -n1 | grep -w "ID" | awk '{print $NF}')
    if [ "AA${id}" = "AA" ]; then
        log_with_time "Loading models to detection..."
        echo -ne "0\n${images_count}\nloading...\n" >"${progress_file}"
        return 1
    fi

    local origin_image=$(get_imagename_by_id "${id}")
    echo -ne "${id}\n${images_count}\n${origin_image}\n" >"${progress_file}"
    if [ "AA${id}" != "AA${images_count}" ]; then
        return 1
    fi

    log_with_time "Done detection."
    return 0
}

get_imagename_by_id() {
    log_with_time "$FUNCNAME start, param: $*"

    local id_name_file="${g_log_fullpath}/rename_to_node_test.sh"
    sed -n "${id}p" "${id_name_file}" | awk '{print $2}'
}

main() {
    log_with_time "$FUNCNAME start, param: $*"
    local task_fullpath=$1
    local models_fullpath="${g_script_fullpath}/peleenet_offline_pruned/models/"
    local node_app_log="${g_log_fullpath}/node_app.temp.log"

    local image_fullpath="${task_fullpath}/Rawdata/Image/"
    local images_count=$(ls "${image_fullpath}"/*.jpg | wc -l)
    log_with_time "There are ${images_count} images in ${image_fullpath}."

    generate_rename_scripts "${task_fullpath}"

    do_rename_to_node_test "${task_fullpath}"
    local progress_file="${g_erlang_soft_fullpath}/.semi_log.txt"
    update_progress_by_node_log "${node_app_log}" "${images_count}" "${progress_file}" &
    do_detection "${models_fullpath}" "${task_fullpath}" "${node_app_log}"
    do_rename_to_origin "${task_fullpath}"

    log_with_time "$FUNCNAME end."
}

main $@
exit $?
