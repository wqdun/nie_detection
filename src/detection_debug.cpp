#include <opencv2/opencv.hpp>
#include <iostream>
#include <map>

#include "lib_public_tools/src/public_tools.h"
#include "lib_public_tools/src/tools_no_ros.h"


class DetectResult {
public:
    DetectResult(int64_t b, int64_t c, int64_t d, int64_t e, int64_t f, double g)
        : detection_x_min(b)
        , detection_y_min(c)
        , detection_x_max(d)
        , detection_y_max(e)

        , detection_class(f)
        , detection_confidence(g) {}


public:
    int64_t detection_x_min;
    int64_t detection_y_min;
    int64_t detection_x_max;
    int64_t detection_y_max;

    int64_t detection_class;
    double detection_confidence;
};


int CreateDebugImage(const std::string &_imagePath, int64_t _frameId, const std::vector<DetectResult> &_detectResult) {
    char frameId[10];
    sprintf(frameId, "%05ld", _frameId);

    const std::string originImageFile(_imagePath + "/node_test_" + frameId + ".jpg");
    LOG(INFO) << "Open: " << originImageFile;
    cv::Mat image = cv::imread(originImageFile);
    if (image.empty()) {
        LOG(ERROR) << "Failed to open: " << originImageFile;
        exit(1);
    }

    for (const auto &detectedRect: _detectResult) {
        cv::rectangle(
            image,
            cv::Point(detectedRect.detection_x_min, detectedRect.detection_y_min),
            cv::Point(detectedRect.detection_x_max, detectedRect.detection_y_max),
            cv::Scalar(0, 0, 255),
            2
        );

        char detectionConfidence[10];
        sprintf(detectionConfidence, "%.2f", detectedRect.detection_confidence);
        cv::putText(
            image,
            std::to_string(detectedRect.detection_class) + "," + detectionConfidence,
            cv::Point(detectedRect.detection_x_min, detectedRect.detection_y_min),
            // font face and scale
            cv::FONT_HERSHEY_COMPLEX, 1,
            cv::Scalar(0, 0, 255),
            // line thickness and type
            1, cv::LINE_AA
        );
    }

    const std::string debugImageFile(_imagePath + "/node_test_" + frameId + "_debug.jpg");
    LOG(INFO) << "Gonna create image: " << debugImageFile;
    cv::imwrite(debugImageFile, image);

    return 0;
}



int main(int argc, char const *argv[]) {
    google::InitGoogleLogging(argv[0]);
    LOG(INFO) << "Built with OpenCV " << CV_VERSION;

    LOG(INFO) << "Got " << argc << " parameters.";
    if (argc < 3) {
        LOG(ERROR) << "./a.out ImagePath ResultTxtFile";
        return 1;
    }

    const std::string imagePath(argv[1]);
    const std::string resultTxtFile(argv[2]);
    std::fstream f(resultTxtFile, std::ios::in);
    if (!f) {
        LOG(ERROR) << "Failed to open: " << resultTxtFile;
        exit(1);
    }

    std::map<int64_t, std::vector<DetectResult>> frameId_DetectResults;
    std::string line;
    while (getline(f, line)) {
        std::vector<std::string> splitLine;
        boost::split(splitLine, line, boost::is_any_of(";"));
        if(7 != splitLine.size() ) {
            LOG(ERROR) << "Wrong line: " << line;
            exit(1);
        }

        frameId_DetectResults[public_tools::ToolsNoRos::string2int64(splitLine[0])].emplace_back(
            public_tools::ToolsNoRos::string2int64(splitLine[1]),
            public_tools::ToolsNoRos::string2int64(splitLine[2]),
            public_tools::ToolsNoRos::string2int64(splitLine[3]),
            public_tools::ToolsNoRos::string2int64(splitLine[4]),
            public_tools::ToolsNoRos::string2int64(splitLine[5]),
            public_tools::ToolsNoRos::string2double(splitLine[6])
        );
    }

    LOG(INFO) << "frameId_DetectResults.size(): " << frameId_DetectResults.size();

    for (const auto &frameId_DetectResult: frameId_DetectResults) {
        CreateDebugImage(imagePath, frameId_DetectResult.first, frameId_DetectResult.second);
    }

    return 0;
}


